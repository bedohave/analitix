import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Input, HostListener, ElementRef } from '@angular/core';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-usermenu',
  templateUrl: './usermenu.component.html',
  styleUrls: ['./usermenu.component.scss']
})
export class UsermenuComponent implements OnInit {
  Username: string;

  @Output() myEvent = new EventEmitter<string>();
  isOpen = false;

  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement) {
    if (!targetElement) {
      return;
    }

    const clickedInside = this.elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
        this.isOpen = false;
    }
  }

  constructor(private elementRef: ElementRef, private authService: AuthService) { }

  ngOnInit() {
    const token = localStorage.getItem('token');
    if (token) {
      this.authService.setDecodedToken(token);
      this.Username = this.authService.decodedToken.unique_name;
    }
  }

  callLogout() {
    this.myEvent.emit('Logout!');
  }

}
