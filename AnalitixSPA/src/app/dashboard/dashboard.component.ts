import { Component, OnInit } from '@angular/core';
import { User } from '../_models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { Clinica } from '../_models/clinica';
import { AuthService } from '../_services/auth.service';
import { AlertService } from '../_services/alert.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  users: User[];
  clinicas: Clinica[];
  clinica: Clinica;

  constructor(private router: Router, private route: ActivatedRoute, private authService: AuthService, private alert: AlertService) { }

  ngOnInit() {
    if (this.authService.decodedToken.role === 'Admin') {
      this.router.navigate(['/dashboard/admin']);
    } else if (this.authService.decodedToken.role === 'Clinica') {
      this.router.navigate(['/dashboard/clinica']);
    } else {
      this.alert.error('Não autorizado!');
      this.router.navigate(['/usuario/editar']);
    }
  }

}
