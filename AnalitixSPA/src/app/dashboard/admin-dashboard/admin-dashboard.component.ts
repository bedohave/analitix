import { Component, OnInit, Input } from '@angular/core';
import { Clinica } from 'src/app/_models/clinica';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  @Input() valuesFromHome: Clinica[];

  constructor() { }

  ngOnInit() {
    console.log(this.valuesFromHome);
  }

}
