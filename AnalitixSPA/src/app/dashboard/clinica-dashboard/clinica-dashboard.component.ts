import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Clinica } from 'src/app/_models/clinica';
import { ActivatedRoute } from '@angular/router';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-clinica-dashboard',
  templateUrl: './clinica-dashboard.component.html',
  styleUrls: ['./clinica-dashboard.component.scss']
})
export class ClinicaDashboardComponent implements OnInit {
  @ViewChild('lineChart') private chartRef;
  @ViewChild('pieChart') private chartRef2;
  chart1: any;
  chart2: any;

  clinica: Clinica;

  labels = ['a', 'b', 'c'];
  dataPoints = [
    { x: 1, y: 3 },
    { x: 2, y: 7 },
    { x: 3, y: 4 }
  ];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.clinica = data['clinica'];
    });
    this.clinica.amostras = [];

    this.chart1 = new Chart(this.chartRef.nativeElement, {
      type: 'line',
      data: {
        labels: this.labels, // your labels array
        datasets: [
          {
            data: this.dataPoints, // your data array
            borderColor: '#00AEFF',
            fill: false
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true,
            ticks: {
              beginAtZero: true
            }
          }],
        }
      }
    });

    this.chart2 = new Chart(this.chartRef2.nativeElement, {
      type: 'doughnut',
      data: {
          labels: ['Data '],
          datasets: [{

              data: [46.97, 46.91, 24.56],

              backgroundColor: [
                  'rgba(255, 99, 132,.7)',
                  'rgba(66, 165, 245,.7)',
                  'rgba(38, 166, 154,.7)',
              ],
          }]
      },
      options: {
          elements: {
              line: {
                  tension: 0.000001
              }
          },
          legend: {
              display: false
          },
          maintainAspectRatio: false,
          responsive: true,
          plugins: {
              filler: {
                  propagate: false
              }
          },
          title: {
              display: true,
              text: 'LEAD GRAPH'
          }
      }

    });
  }

}
