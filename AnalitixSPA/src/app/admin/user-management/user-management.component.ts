import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/_models/user';
import { AdminService } from 'src/app/_services/admin.service';
import { AlertService } from 'src/app/_services/alert.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialogConfig, MatDialog } from '@angular/material';
import { RolesDialogComponent } from 'src/app/dialogs/roles-dialog/roles-dialog.component';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
  users: User[];
  displayedColumns: string[] = ['id', 'userName', 'roles', 'actions'];
  dataSource: MatTableDataSource<User>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  private rolesConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: true
  };

  constructor(private adminService: AdminService, private alert: AlertService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getUsersWithRoles();
  }

  getUsersWithRoles() {
    this.adminService.getUsersWithRoles().subscribe((users: User[]) => {
      this.users = users;
    }, error => {
      this.alert.error(error);
    }, () => {
      this.dataSource = new MatTableDataSource(this.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editRoles(user: User) {
    const rolesConfig = this.rolesConfig;
    rolesConfig.data = {user, roles: this.getRolesArray(user)};
    const dialogRef = this.dialog.open(RolesDialogComponent, rolesConfig);
    dialogRef.afterClosed().subscribe(values => {
      if (values === undefined) {
        console.log('cancelado');
        return;
      }
      const rolesToUpdate = {
        roleNames: [...values.filter(el => el.checked === true).map(el => el.name)]
      };
      if (rolesToUpdate) {
        this.adminService.updateUserRoles(user, rolesToUpdate).subscribe(() => {
          user.roles = [...rolesToUpdate.roleNames];
        }, error => {
          this.alert.error(error);
        });
      }
    });
  }

  private getRolesArray(user) {
    const rolesList = [];
    const userRoles = user.roles;
    let availableRoles: any[] = [];
    this.adminService.getRolesNames().subscribe((roles: any[]) => {
      availableRoles = roles;
    }, error => {
      console.log(error);
    }, () => {
      for (let i = 0; i < availableRoles.length; i++) {
        let isMatch = false;
        for (let j = 0; j < userRoles.length; j++) {
          if (availableRoles[i].name === userRoles[j]) {
            isMatch = true;
            availableRoles[i].checked = true;
            rolesList.push(availableRoles[i]);
            break;
          }
        }
        if (!isMatch) {
          availableRoles[i].checked = false;
          rolesList.push(availableRoles[i]);
        }
      }
    });
    return rolesList;
  }

}
