import { Component, OnInit, ViewChild } from '@angular/core';
import { Motoboy } from '../_models/motoboy';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-motoboys',
  templateUrl: './motoboys.component.html',
  styleUrls: ['./motoboys.component.scss']
})
export class MotoboysComponent implements OnInit {
  motoboys: Motoboy[];
  displayedColumns: string[] = ['nome', 'cpf', 'celular'];
  dataSource: MatTableDataSource<Motoboy>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.motoboys = data['motoboys'];
    });
    this.dataSource = new MatTableDataSource(this.motoboys);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
