/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MotoboysComponent } from './motoboys.component';

describe('MotoboysComponent', () => {
  let component: MotoboysComponent;
  let fixture: ComponentFixture<MotoboysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MotoboysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MotoboysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
