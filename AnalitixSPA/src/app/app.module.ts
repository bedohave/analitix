import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';
import { MaterialModule } from './material.module';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { AuthService } from './_services/auth.service';
import { UsermenuComponent } from './usermenu/usermenu.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ErrorInterceptorProvider } from './_services/error.interceptor';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { MedicosComponent } from './medicos/medicos.component';
import { ClinicasComponent } from './clinicas/clinicas.component';
import { LaboratoriosComponent } from './laboratorios/laboratorios.component';
import { appRoutes } from './routes';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminDashboardComponent } from './dashboard/admin-dashboard/admin-dashboard.component';
import { ClinicaDashboardComponent } from './dashboard/clinica-dashboard/clinica-dashboard.component';
import { MedicoDashboardComponent } from './dashboard/medico-dashboard/medico-dashboard.component';
import { PacienteDashboardComponent } from './dashboard/paciente-dashboard/paciente-dashboard.component';
import { AuthGuard } from './_guards/auth.guard';
import { UserService } from './_services/user.service';
import { UserDetailResolver } from './_resolvers/user-detail.resolver';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserListResolver } from './_resolvers/user-list.resolver';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserEditResolver } from './_resolvers/user-edit.resolver';
import { PreventUnsavedChanges } from './_guards/prevent-unsaved-changes.guard';
import { UploadPdfComponent } from './upload-pdf/upload-pdf.component';
import { AdminPanelComponent } from './admin/admin-panel/admin-panel.component';
import { HasRoleDirective } from './_directives/hasRole.directive';
import { UserManagementComponent } from './admin/user-management/user-management.component';
import { AdminService } from './_services/admin.service';
import { RolesDialogComponent } from './dialogs/roles-dialog/roles-dialog.component';
import { LoginComponent } from './login/login.component';
import { ClinicaRegisterComponent } from './register/clinica-register/clinica-register.component';
import { SelectBancoComponent } from './selects/select-banco/select-banco.component';
import { PacienteRegisterComponent } from './register/paciente-register/paciente-register.component';
import { MedicoRegisterComponent } from './register/medico-register/medico-register.component';
import { ClinicaAdminListResolver } from './_resolvers/clinica-admin-list.resolver';
import { ClinicaDashboardResolver } from './_resolvers/clinica-dashboard.resolver';
import { MedicoListResolver } from './_resolvers/medico-list.resolver';
import { PacientesComponent } from './pacientes/pacientes.component';
import { PacienteListResolver } from './_resolvers/paciente-list.resolver';
import { MotoboysComponent } from './motoboys/motoboys.component';
import { MotoboyListResolver } from './_resolvers/motoboy-list.resolver';
import { LaboratorioListResolver } from './_resolvers/laboratorio-list.resolver';

import { MatPaginatorIntlPtBr } from './_configs/matPaginatorIntlPtBr';
import { MatPaginatorIntl } from '@angular/material';
import { AddLaboratorioDialogComponent } from './dialogs/add-laboratorio-dialog/add-laboratorio-dialog.component';
import { AddMedicoDialogComponent } from './dialogs/add-medico-dialog/add-medico-dialog.component';

export function tokenGetter() {
    return localStorage.getItem('token');
}

@NgModule({
   declarations: [
      AppComponent,
      NavComponent,
      SidemenuComponent,
      UsermenuComponent,
      HomeComponent,
      RegisterComponent,
      ClinicaRegisterComponent,
      PacienteRegisterComponent,
      MedicoRegisterComponent,
      ConfirmDialogComponent,
      MedicosComponent,
      ClinicasComponent,
      LaboratoriosComponent,
      DashboardComponent,
      AdminDashboardComponent,
      ClinicaDashboardComponent,
      MedicoDashboardComponent,
      PacienteDashboardComponent,
      UserDetailComponent,
      UserEditComponent,
      UploadPdfComponent,
      AdminPanelComponent,
      HasRoleDirective,
      UserManagementComponent,
      RolesDialogComponent,
      LoginComponent,
      SelectBancoComponent,
      PacientesComponent,
      MotoboysComponent,
      AddLaboratorioDialogComponent,
      AddMedicoDialogComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule,
      BrowserAnimationsModule,
      MaterialModule,
      FormsModule,
      ReactiveFormsModule,
      RouterModule.forRoot(appRoutes),
      JwtModule.forRoot({
        config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:5000'],
        blacklistedRoutes: ['localhost:5000/api/auth']
        }
      })
    ],
   providers: [
      AuthService,
      ErrorInterceptorProvider,
      AuthGuard,
      UserService,
      UserDetailResolver,
      UserListResolver,
      UserEditResolver,
      PreventUnsavedChanges,
      AdminService,
      ClinicaAdminListResolver,
      ClinicaDashboardResolver,
      MedicoListResolver,
      PacienteListResolver,
      MotoboyListResolver,
      LaboratorioListResolver,
      { provide: MatPaginatorIntl, useClass: MatPaginatorIntlPtBr }
   ],
   bootstrap: [
      AppComponent
   ],
   entryComponents: [
      ConfirmDialogComponent,
      RolesDialogComponent,
      AddLaboratorioDialogComponent,
      AddMedicoDialogComponent
   ]
})
export class AppModule { }
