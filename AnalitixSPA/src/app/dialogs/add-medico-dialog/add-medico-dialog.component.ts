import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Medico } from 'src/app/_models/medico';

@Component({
  selector: 'app-add-medico-dialog',
  templateUrl: './add-medico-dialog.component.html',
  styleUrls: ['./add-medico-dialog.component.scss']
})
export class AddMedicoDialogComponent implements OnInit {
  medicoForm: FormGroup;

  constructor(private dialogRef: MatDialogRef<AddMedicoDialogComponent>, private fb: FormBuilder) { }

  ngOnInit() {
    this.createMedicoForm();
  }

  createMedicoForm() {
    this.medicoForm = this.fb.group({
      user: this.fb.group({
        email: ['', [Validators.required, Validators.email]]
      }),
      nome: ['', [Validators.required, Validators.minLength(2)]],
      registro: ['', Validators.required],
      especialidade: ['', Validators.required],
      cidade: ['', Validators.required],
      estado: ['', Validators.required]
    });
  }

  cadastrarMedico() {
    const random = Math.round((Math.random() * 100) * 100);
    const medicoForRegister: Medico = Object.assign({}, this.medicoForm.value, {
      user: {
        password: 'temp' + random,
        userName: 'temp' + random,
        email: this.medicoForm.get('user.email').value
      }
    });
    this.dialogRef.close(medicoForRegister);
  }

  fechar() {
    this.dialogRef.close();
  }

}
