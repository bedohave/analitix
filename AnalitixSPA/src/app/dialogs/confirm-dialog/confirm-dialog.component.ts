import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {
  message: string;

  constructor(
    private dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) {message}) {
      this.message = message;
    }

  ngOnInit() {
  }

  save() {
    this.dialogRef.close('confirmado');
    this.dialogRef.afterClosed().subscribe(x => {
      console.log('Dialog output', x);
    });
  }

  close() {
    this.dialogRef.close('cancelado');
    this.dialogRef.afterClosed().subscribe(x => {
      console.log('Dialog output', x);
    });
  }

}
