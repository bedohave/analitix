import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialogConfig } from '@angular/material';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CepService } from 'src/app/_services/cep.service';
import { Laboratorio } from 'src/app/_models/laboratorio';

@Component({
  selector: 'app-add-laboratorio-dialog',
  templateUrl: './add-laboratorio-dialog.component.html',
  styleUrls: ['./add-laboratorio-dialog.component.scss']
})
export class AddLaboratorioDialogComponent implements OnInit {
  laboratorioForm: FormGroup;

  constructor(private dialogRef: MatDialogRef<AddLaboratorioDialogComponent>, private fb: FormBuilder, private cep: CepService) { }

  ngOnInit() {
    this.createLaboratorioForm();
  }

  createLaboratorioForm() {
    this.laboratorioForm = this.fb.group({
      user: this.fb.group({
        email: ['', [Validators.required, Validators.email]]
      }),
      nome: ['', Validators.required],
      telefone: ['', Validators.required],
      cep: ['', Validators.required],
      endereco: ['', Validators.required],
      bairro: ['', Validators.required],
      cidade: ['', Validators.required],
      estado: ['', Validators.required]
    });
  }

  cadastrarLaboratorio() {
    const random = Math.round((Math.random() * 100) * 100);
    const laboratorioForRegister: Laboratorio = Object.assign({}, this.laboratorioForm.value, {
      user: {
        password: 'temp' + random,
        userName: 'temp' + random,
        email: this.laboratorioForm.get('user.email').value
      }
    });
    this.dialogRef.close(laboratorioForRegister);
  }

  fechar() {
    this.dialogRef.close();
  }

  buscaCep(cep: string) {
    cep = cep.replace(/\D/g, '');
    if (cep != null && cep !== '') {
      this.cep.consultaCEP(cep).subscribe(dados => this.populaCep(dados, this.laboratorioForm));
    }
  }

  populaCep(dados, formulario: FormGroup) {
    formulario.patchValue({
      endereco: dados.logradouro + ', ',
      bairro: dados.bairro,
      cidade: dados.localidade,
      estado: dados.uf
    });
  }

}
