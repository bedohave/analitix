import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/_models/user';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-roles-dialog',
  templateUrl: './roles-dialog.component.html',
  styleUrls: ['./roles-dialog.component.scss']
})
export class RolesDialogComponent implements OnInit {
  @Output() updateSelectedRoles = new EventEmitter();
  user: User;
  roles: any[];

  constructor(private dialogRef: MatDialogRef<RolesDialogComponent>, @Inject(MAT_DIALOG_DATA) {user, roles}) {
    this.user = user;
    this.roles = roles;
  }

  ngOnInit() {
  }

  updateRoles() {
    this.updateSelectedRoles.emit(this.roles);
    this.dialogRef.close(this.roles);
  }

  save() {
    this.dialogRef.close('confirmado');
    this.dialogRef.afterClosed().subscribe(x => {
      console.log('Dialog output', x);
    });
  }

  close() {
    this.dialogRef.close();
  }

}
