import { Injectable } from '@angular/core';
import { Motoboy } from '../_models/motoboy';
import { AuthService } from '../_services/auth.service';
import { Router, ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { AlertService } from '../_services/alert.service';
import { ClinicaService } from '../_services/clinica.service';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';

@Injectable()
export class MotoboyListResolver implements Resolve<Motoboy[]> {
    constructor(private authService: AuthService, private router: Router, private alert: AlertService,
        private clinicaService: ClinicaService) {}
    resolve(route: ActivatedRouteSnapshot): Observable<Motoboy[]> {
        return this.clinicaService.getMotoboysFromClinica(this.authService.decodedToken.nameid).pipe(
            catchError(error => {
                this.alert.error('Token inválido!');
                localStorage.removeItem('token');
                this.router.navigate(['/home']);
                return of(null);
            })
        );
    }
}
