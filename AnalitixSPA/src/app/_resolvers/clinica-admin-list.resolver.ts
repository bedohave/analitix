import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Clinica } from '../_models/clinica';
import { AdminService } from '../_services/admin.service';
import { AlertService } from '../_services/alert.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ClinicaAdminListResolver implements Resolve<Clinica[]> {
    constructor(private adminService: AdminService, private router: Router, private alert: AlertService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Clinica[]> {
        return this.adminService.getClinicas().pipe(
            catchError(error => {
                this.alert.error('Token inválido!');
                localStorage.removeItem('token');
                this.router.navigate(['/home']);
                return of(null);
            })
        );
    }
}
