import { Injectable } from '@angular/core';
import { Clinica } from '../_models/clinica';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { ClinicaService } from '../_services/clinica.service';
import { AlertService } from '../_services/alert.service';
import { AuthService } from '../_services/auth.service';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';

@Injectable()
export class ClinicaDashboardResolver implements Resolve<Clinica> {
    constructor(private clinicaService: ClinicaService, private router: Router, private alert: AlertService,
        private authService: AuthService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Clinica> {
        return this.clinicaService.getClinica(this.authService.decodedToken.nameid).pipe(
            catchError(error => {
                this.alert.error('Token inválido!');
                localStorage.removeItem('token');
                this.router.navigate(['/home']);
                return of(null);
            })
        );
    }
}
