import { Injectable } from '@angular/core';
import { Medico } from '../_models/medico';
import { AuthService } from '../_services/auth.service';
import { Router, ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { AlertService } from '../_services/alert.service';
import { ClinicaService } from '../_services/clinica.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class MedicoListResolver implements Resolve<Medico[]> {
    constructor(private authService: AuthService, private router: Router, private alert: AlertService,
        private clinicaService: ClinicaService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Medico[]> {
        return this.clinicaService.getMedicosFromClinica(this.authService.decodedToken.nameid).pipe(
            catchError(error => {
                this.alert.error('Token inválido!');
                localStorage.removeItem('token');
                this.router.navigate(['/home']);
                return of(null);
            })
        );
    }
}
