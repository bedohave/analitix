import { Injectable } from '@angular/core';
import { Laboratorio } from '../_models/laboratorio';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { AlertService } from '../_services/alert.service';
import { ClinicaService } from '../_services/clinica.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class LaboratorioListResolver implements Resolve<Laboratorio[]> {
    constructor(private authService: AuthService, private router: Router, private alert: AlertService,
        private clinicaService: ClinicaService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Laboratorio[]> {
        return this.clinicaService.getLaboratoriosFromClinica(this.authService.decodedToken.nameid).pipe(
            catchError(error => {
                this.alert.error('Token inválido!');
                localStorage.removeItem('token');
                this.router.navigate(['/home']);
                return of(null);
            })
        );
    }
}
