import { User } from './user';

export class Medico {
    user: User;
    nome: string;
    registro: string;
    especialidade: string;
    cidade: string;
    estado: string;
}
