import { User } from './user';

export interface Motoboy {
    user: User;
    nome: string;
    cpf: string;
    celular: string;
}
