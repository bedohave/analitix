import { User } from './user';

export interface Laboratorio {
    user: User;
    nome: string;
    telefone: string;
    cep: string;
    endereco: string;
    bairro: string;
    cidade: string;
    estado: string;
}
