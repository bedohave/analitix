import { User } from './user';
import { Medico } from './medico';
import { Paciente } from './paciente';
import { Amostra } from './amostra';
import { Laboratorio } from './laboratorio';
import { Motoboy } from './motoboy';

export interface Clinica {
    user: User;
    nome: string;
    razaoSocial: string;
    cpf: string;
    cnpj: string;
    banco: string;
    agencia: string;
    conta: string;
    cep: string;
    endereco: string;
    bairro: string;
    cidade: string;
    estado: string;
    laboratorios: Laboratorio[];
    medicos: Medico[];
    motoboys: Motoboy[];
    pacientes: Paciente[];
    amostras: Amostra[];
}
