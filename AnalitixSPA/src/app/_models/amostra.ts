export interface Amostra {
    id: number;
    medico: string;
    tipo: string;
    codigo: string;
    dataColeta: Date;
}
