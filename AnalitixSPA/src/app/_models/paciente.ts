import { User } from './user';

export class Paciente {
    user: User;
    nome: string;
    cpf: string;
    sexo: string;
    nascimento: Date;
    cep: string;
    endereco: string;
    bairro: string;
    cidade: string;
    estado: string;
    celular: string;
}
