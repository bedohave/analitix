import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MedicosComponent } from './medicos/medicos.component';
import { LaboratoriosComponent } from './laboratorios/laboratorios.component';
import { ClinicasComponent } from './clinicas/clinicas.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './_guards/auth.guard';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserDetailResolver } from './_resolvers/user-detail.resolver';
import { UserListResolver } from './_resolvers/user-list.resolver';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserEditResolver } from './_resolvers/user-edit.resolver';
import { PreventUnsavedChanges } from './_guards/prevent-unsaved-changes.guard';
import { UploadPdfComponent } from './upload-pdf/upload-pdf.component';
import { AdminPanelComponent } from './admin/admin-panel/admin-panel.component';
import { LoginComponent } from './login/login.component';
import { ClinicaAdminListResolver } from './_resolvers/clinica-admin-list.resolver';
import { ClinicaDashboardResolver } from './_resolvers/clinica-dashboard.resolver';
import { AdminDashboardComponent } from './dashboard/admin-dashboard/admin-dashboard.component';
import { ClinicaDashboardComponent } from './dashboard/clinica-dashboard/clinica-dashboard.component';
import { MedicoListResolver } from './_resolvers/medico-list.resolver';
import { PacientesComponent } from './pacientes/pacientes.component';
import { PacienteListResolver } from './_resolvers/paciente-list.resolver';
import { MotoboysComponent } from './motoboys/motoboys.component';
import { LaboratorioListResolver } from './_resolvers/laboratorio-list.resolver';
import { MotoboyListResolver } from './_resolvers/motoboy-list.resolver';

export const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    {
        path: '',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuard],
        children: [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'dashboard/admin', component: AdminDashboardComponent,
                resolve: { users: UserListResolver, clinicas: ClinicaAdminListResolver } },
            { path: 'dashboard/clinica', component: ClinicaDashboardComponent, resolve: { clinica: ClinicaDashboardResolver } },
            { path: 'usuarios/:id', component: UserDetailComponent, resolve: {user: UserDetailResolver} },
            { path: 'usuario/editar', component: UserEditComponent, resolve: {user: UserEditResolver},
                canDeactivate: [PreventUnsavedChanges] },
            { path: 'clinicas', component: ClinicasComponent },
            { path: 'laboratorios', component: LaboratoriosComponent, resolve: {laboratorios: LaboratorioListResolver} },
            { path: 'medicos', component: MedicosComponent, resolve: {medicos: MedicoListResolver} },
            { path: 'motoboys', component: MotoboysComponent, resolve: {motoboys: MotoboyListResolver} },
            { path: 'pacientes', component: PacientesComponent, resolve: {pacientes: PacienteListResolver} },
            { path: 'uploadPdf', component: UploadPdfComponent },
            { path: 'admin', component: AdminPanelComponent, data: {roles: ['Admin', 'Moderator']} },
            { path: 'login', component: LoginComponent }
        ]
    },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];
