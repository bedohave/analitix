import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CepService } from 'src/app/_services/cep.service';
import { BancoService } from 'src/app/_services/banco.service';

@Component({
  selector: 'app-clinica-register',
  templateUrl: './clinica-register.component.html',
  styleUrls: ['./clinica-register.component.scss']
})
export class ClinicaRegisterComponent implements OnInit {
  @Output() dadosClinica = new EventEmitter();
  clinicaForm: FormGroup;
  clinicaTipo = 1;
  bancos: any[];

  constructor(private fb: FormBuilder, private cep: CepService, private banco: BancoService) { }

  ngOnInit() {
    this.createClinicaForm();
    this.clinicaForm.statusChanges.subscribe(() => {
      this.dadosClinica.emit(this.clinicaForm);
    });
  }

  createClinicaForm() {
    this.clinicaForm = this.fb.group({
      nome: ['', [Validators.required, Validators.minLength(2)]],
      cpf: ['', [Validators.required, Validators.minLength(5)]],
      banco: ['', Validators.required],
      agencia: ['', Validators.required],
      conta: ['', Validators.required],
      cep: ['', Validators.required],
      endereco: ['', Validators.required],
      bairro: ['', Validators.required],
      cidade: ['', Validators.required],
      estado: ['', Validators.required]
    });
    this.bancos = this.banco.getBancos();
  }

  setValidator(e) {
    this.clinicaTipo = e.value;
    const group = this.clinicaForm as FormGroup;
    if (e.value === 1) {
      group.addControl('nome', this.fb.control('', [Validators.required, Validators.minLength(2)]));
      group.addControl('cpf', this.fb.control('', [Validators.required, Validators.minLength(5)]));
      group.removeControl('razaoSocial');
      group.removeControl('cnpj');

    } else if (e.value === 2) {
      group.addControl('razaoSocial', this.fb.control('', [Validators.required, Validators.minLength(2)]));
      group.addControl('cnpj', this.fb.control('', [Validators.required, Validators.minLength(6)]));
      group.removeControl('nome');
      group.removeControl('cpf');
    }
  }

  buscaCep(cep: string) {
    cep = cep.replace(/\D/g, '');
    if (cep != null && cep !== '') {
      this.cep.consultaCEP(cep).subscribe(dados => this.populaCep(dados, this.clinicaForm));
    }
  }

  populaCep(dados, formulario: FormGroup) {
    formulario.patchValue({
      endereco: dados.logradouro + ', ',
      bairro: dados.bairro,
      cidade: dados.localidade,
      estado: dados.uf
    });
  }
}
