import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CepService } from 'src/app/_services/cep.service';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from './date.adapter';

@Component({
  selector: 'app-paciente-register',
  templateUrl: './paciente-register.component.html',
  styleUrls: ['./paciente-register.component.scss'],
  providers: [
    {
      provide: DateAdapter, useClass: AppDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class PacienteRegisterComponent implements OnInit {
  @Output() dadosPaciente = new EventEmitter();
  pacienteForm: FormGroup;

  constructor(private fb: FormBuilder, private cep: CepService) { }

  ngOnInit() {
    this.createPacienteForm();
    this.pacienteForm.statusChanges.subscribe(() => {
      this.dadosPaciente.emit(this.pacienteForm);
    });
  }

  createPacienteForm() {
    this.pacienteForm = this.fb.group({
      nome: ['', [Validators.required, Validators.minLength(2)]],
      cpf: ['', [Validators.required, Validators.minLength(5)]],
      sexo: ['', Validators.required],
      nascimento: ['', Validators.required],
      cep: ['', Validators.required],
      endereco: ['', Validators.required],
      bairro: ['', Validators.required],
      cidade: ['', Validators.required],
      estado: ['', Validators.required],
      celular: ['', Validators.required]
    });
  }

  buscaCep(cep: string) {
    cep = cep.replace(/\D/g, '');
    if (cep != null && cep !== '') {
      this.cep.consultaCEP(cep).subscribe(dados => this.populaCep(dados, this.pacienteForm));
    }
  }

  populaCep(dados, formulario: FormGroup) {
    formulario.patchValue({
      endereco: dados.logradouro + ', ',
      bairro: dados.bairro,
      cidade: dados.localidade,
      estado: dados.uf
    });
  }

}
