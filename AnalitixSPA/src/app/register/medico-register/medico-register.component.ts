import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-medico-register',
  templateUrl: './medico-register.component.html',
  styleUrls: ['./medico-register.component.scss']
})
export class MedicoRegisterComponent implements OnInit {
  @Output() dadosMedico = new EventEmitter();
  medicoForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.createMedicoForm();
    this.medicoForm.statusChanges.subscribe(() => {
      this.dadosMedico.emit(this.medicoForm);
    });
  }

  createMedicoForm() {
    this.medicoForm = this.fb.group({
      nome: ['', [Validators.required, Validators.minLength(2)]],
      registro: ['', Validators.required],
      especialidade: ['', Validators.required],
      cidade: ['', Validators.required],
      estado: ['', Validators.required]
    });
  }

}
