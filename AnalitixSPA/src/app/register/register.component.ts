import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertService } from '../_services/alert.service';
import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { Router } from '@angular/router';
import { Clinica } from '../_models/clinica';
import { Paciente } from '../_models/paciente';
import { Medico } from '../_models/medico';

export class ParentErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = !!(form && form.submitted);
    const controlTouched = !!(control && (control.dirty || control.touched));
    const controlInvalid = !!(control && control.invalid);
    const parentInvalid = !!(control && control.parent && control.parent.invalid && (control.parent.dirty || control.parent.touched));

    return isSubmitted || (controlTouched && (controlInvalid || parentInvalid));
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @Output() cancelRegister = new EventEmitter();
  registerForm: FormGroup;
  userForm: FormGroup;
  tempForm: FormGroup;
  matcher = new ParentErrorStateMatcher();

  step = 0;
  registerUnlock = false;
  tipoCadastro = '';

  dadosFormStatus = false;

  constructor(private authService: AuthService, private alert: AlertService, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.createUserForm();
  }

  createUserForm() {
    this.userForm = this.fb.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      passwords: this.fb.group({
        password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(8)]],
        confirmPassword: ['', Validators.required]
      }, {validator: this.checkPasswords})
    });
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true };
  }

  register() {
    if (this.registerForm.valid) {
      if (this.tipoCadastro === 'clinica') {
        const clinicaForRegister: Clinica = Object.assign({}, this.registerForm.value);
        this.authService.registerClinica(clinicaForRegister).subscribe(() => {
          this.alert.success('Registration successful!');
        }, error => {
          this.alert.error(error);
          console.log(error);
        }, () => {
          this.authService.login(clinicaForRegister.user).subscribe(() => {
            this.router.navigate(['/dashboard']);
          });
          this.cancel();
        });
      } else if (this.tipoCadastro === 'medico') {
        const medicoForRegister: Medico = Object.assign({}, this.registerForm.value);
        this.authService.registerMedico(medicoForRegister).subscribe(() => {
          this.alert.success('Registro concluído!');
        }, error => {
          this.alert.error(error);
        }, () => {
          this.authService.login(medicoForRegister.user).subscribe(() => {
            this.router.navigate(['/dashboard']);
          });
          this.cancel();
        });
      } else if (this.tipoCadastro === 'paciente') {
        const pacienteForRegister: Paciente = Object.assign({}, this.registerForm.value);
        this.authService.registerPaciente(pacienteForRegister).subscribe(() => {
          this.alert.success('Registro concluído!');
        }, error => {
          this.alert.error(error);
        }, () => {
          this.authService.login(pacienteForRegister.user).subscribe(() => {
            this.router.navigate(['/dashboard']);
          });
          this.cancel();
        });
      }
    }
  }

  cancel() {
    this.cancelRegister.emit(false);
  }

  setStep(index: number) {
    this.step = index;
    if (index === 1) {
      this.registerForm = this.userForm;
    }
  }

  nextStep() {
    this.step++;
    this.registerForm = this.userForm;
  }

  prevStep() {
    this.step--;
  }

  setDados(form: FormGroup) {
    this.dadosFormStatus = form.valid;
    this.tempForm = form;
    this.registerForm = this.fb.group({
      user: this.fb.group({
        username: this.userForm.get('username').value,
        password: this.userForm.get('passwords.password').value,
        email: this.userForm.get('email').value
      }),
      ...this.tempForm.value
    });
  }

  concluir() {
    this.step++;
    this.registerUnlock = true;
  }

}
