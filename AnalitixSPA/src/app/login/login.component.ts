import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertService } from '../_services/alert.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  model: any = {};

  constructor(private authService: AuthService, private alert: AlertService, private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
    this.createLoginForm();
  }

  createLoginForm() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(8)]]
    });
  }

  login() {
    this.authService.login(this.model).subscribe(next => {
      this.alert.success('Logado com sucesso!');
    }, error => {
      this.alert.error(error);
    }, () => {
      this.router.navigate(['/dashboard']);
    });
  }
}
