import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpParams, HttpRequest, HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UploadPdfService {

  constructor(private http: HttpClient) { }

  uploadFile(url: string, file: File): Observable<HttpEvent<any>> {

    const formData = new FormData();
    formData.append('upload', file);

    const params = new HttpParams();

    const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('token') });

    const options = {
      params: params,
      reportProgress: true,
      headers: headers
    };

    const req = new HttpRequest('POST', url, formData, options);
    return this.http.request(req);
  }

}
