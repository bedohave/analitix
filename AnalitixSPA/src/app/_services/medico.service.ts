import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getClinica(id: number) {
    return this.http.get(this.baseUrl);
  }

}
