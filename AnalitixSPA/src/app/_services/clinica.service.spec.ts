/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ClinicaService } from './clinica.service';

describe('Service: Clinica', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClinicaService]
    });
  });

  it('should ...', inject([ClinicaService], (service: ClinicaService) => {
    expect(service).toBeTruthy();
  }));
});
