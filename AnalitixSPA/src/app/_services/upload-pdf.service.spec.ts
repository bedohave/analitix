/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UploadPdfService } from './upload-pdf.service';

describe('Service: UploadPdf', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UploadPdfService]
    });
  });

  it('should ...', inject([UploadPdfService], (service: UploadPdfService) => {
    expect(service).toBeTruthy();
  }));
});
