import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from '../../environments/environment';
import { Medico } from '../_models/medico';
import { Clinica } from '../_models/clinica';
import { Paciente } from '../_models/paciente';
import { Laboratorio } from '../_models/laboratorio';
import { Motoboy } from '../_models/motoboy';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.apiUrl + 'auth/';
  jwtHelper = new JwtHelperService();
  decodedToken: any;

  constructor(private http: HttpClient) { }

  login(model: any) {
    return this.http.post(this.baseUrl + 'login', model).pipe(
      map((response: any) => {
        const user = response;
        if (user) {
          localStorage.setItem('token', user.token);
          this.decodedToken = this.jwtHelper.decodeToken(user.token);
        }
      })
    );
  }

  setDecodedToken(token: any) {
    this.decodedToken = this.jwtHelper.decodeToken(token);
  }

  registerClinica(clinica: Clinica) {
    return this.http.post(this.baseUrl + 'register/clinica', clinica);
  }

  registerLaboratorio(laboratorio: Laboratorio) {
    return this.http.post(this.baseUrl + 'register/laboratorio', laboratorio);
  }

  registerMedico(medico: Medico) {
    return this.http.post(this.baseUrl + 'register/medico', medico);
  }

  registerMotoboy(motoboy: Motoboy) {
    return this.http.post(this.baseUrl + 'register/motoboy', motoboy);
  }

  registerPaciente(paciente: Paciente) {
    return this.http.post(this.baseUrl + 'register/paciente', paciente);
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  roleMatch(allowedRoles): boolean {
    let isMatch = false;
    // const userRoles = this.decodedToken.role as Array<string>;
    const userRoles = this.jwtHelper.decodeToken(localStorage.getItem('token')).role as Array<string>;
    allowedRoles.forEach(element => {
      if (userRoles.includes(element)) {
        isMatch = true;
        return;
      }
    });
    return isMatch;
  }
}
