import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  private confirmConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: true
  };

  private toastConfig: MatSnackBarConfig = {
    horizontalPosition: 'right',
    verticalPosition: 'bottom',
    duration: 3000
  };

  private successConfig: MatSnackBarConfig = {
    horizontalPosition: this.toastConfig.horizontalPosition,
    verticalPosition: this.toastConfig.verticalPosition,
    duration: this.toastConfig.duration,
    panelClass: 'success-toast'
  };

  private errorConfig: MatSnackBarConfig = {
    horizontalPosition: this.toastConfig.horizontalPosition,
    verticalPosition: this.toastConfig.verticalPosition,
    duration: this.toastConfig.duration,
    panelClass: 'error-toast'
  };

  private warningConfig: MatSnackBarConfig = {
    horizontalPosition: this.toastConfig.horizontalPosition,
    verticalPosition: this.toastConfig.verticalPosition,
    duration: this.toastConfig.duration,
    panelClass: 'warning-toast'
  };

  private messageConfig: MatSnackBarConfig = {
    horizontalPosition: this.toastConfig.horizontalPosition,
    verticalPosition: this.toastConfig.verticalPosition,
    duration: this.toastConfig.duration,
    panelClass: 'message-toast'
  };

  constructor(private dialog: MatDialog, private toast: MatSnackBar) { }

  confirm(message: string) {
    const confirmConfig = this.confirmConfig;
    confirmConfig.data = { message };
    this.dialog.open(ConfirmDialogComponent, confirmConfig);
  }

  success(message: string) {
    this.toast.open(message, null, this.successConfig);
  }
  error(message: string) {
    this.toast.open(message, null, this.errorConfig);
  }
  warning(message: string) {
    this.toast.open(message, null, this.warningConfig);
  }
  message(message: string) {
    this.toast.open(message, null, this.messageConfig);
  }

}
