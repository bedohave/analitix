import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Clinica } from '../_models/clinica';
import { Observable } from 'rxjs';
import { Medico } from '../_models/medico';
import { Paciente } from '../_models/paciente';
import { Motoboy } from '../_models/motoboy';
import { Laboratorio } from '../_models/laboratorio';

@Injectable({
  providedIn: 'root'
})
export class ClinicaService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getClinica(id): Observable<Clinica> {
    return this.http.get<Clinica>(this.baseUrl + 'clinica/' + id);
  }

  getLaboratoriosFromClinica(id): Observable<Laboratorio[]> {
    return this.http.get<Laboratorio[]>(this.baseUrl + 'clinica/laboratorios/' + id);
  }

  getMedicosFromClinica(id): Observable<Medico[]> {
    return this.http.get<Medico[]>(this.baseUrl + 'clinica/medicos/' + id);
  }

  getMotoboysFromClinica(id): Observable<Motoboy[]> {
    return this.http.get<Motoboy[]>(this.baseUrl + 'clinica/motoboys/' + id);
  }

  getPacientesFromClinica(id): Observable<Paciente[]> {
    return this.http.get<Paciente[]>(this.baseUrl + 'clinica/pacientes/' + id);
  }

  registerLaboratorio(laboratorio: Laboratorio, id: number) {
    return this.http.post(this.baseUrl + 'clinica/laboratorio/' + id, laboratorio);
  }

  registerMedico(medico: Medico, id: number) {
    return this.http.post(this.baseUrl + 'clinica/medico/' + id, medico);
  }

  registerPaciente(paciente: Paciente, id: number) {
    return this.http.post(this.baseUrl + 'clinica/paciente/' + id, paciente);
  }

}
