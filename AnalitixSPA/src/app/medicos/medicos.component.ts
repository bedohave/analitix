import { Component, OnInit, ViewChild } from '@angular/core';
import { Medico } from '../_models/medico';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSort, MatDialogConfig, MatDialog } from '@angular/material';
import { AddMedicoDialogComponent } from '../dialogs/add-medico-dialog/add-medico-dialog.component';
import { AlertService } from '../_services/alert.service';
import { ClinicaService } from '../_services/clinica.service';
import { AuthService } from '../_services/auth.service';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.component.html',
  styleUrls: ['./medicos.component.scss']
})
export class MedicosComponent implements OnInit {
  medicos: Medico[];
  displayedColumns: string[] = ['nome', 'registro', 'especialidade', 'cidade', 'estado'];
  dataSource: MatTableDataSource<Medico>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  private addMedicoConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: true
  };

  constructor(private route: ActivatedRoute, private dialog: MatDialog, private alert: AlertService,
    private clinicaService: ClinicaService, private authService: AuthService, breakpointObserver: BreakpointObserver) {
      breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
        this.displayedColumns = result.matches ?
            ['nome', 'especialidade'] :
            ['nome', 'registro', 'especialidade', 'cidade', 'estado'];
      });
    }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.medicos = data['medicos'];
    });
    this.dataSource = new MatTableDataSource(this.medicos);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  adicionarMedico() {
    const addLaboratorioConfig = this.addMedicoConfig;
    const dialogRef = this.dialog.open(AddMedicoDialogComponent, addLaboratorioConfig);
    dialogRef.afterClosed().subscribe(values => {
      if (values === undefined) {
        this.alert.message('Cadastro cancelado');
        return;
      }
      const medicoToRegister: Medico = Object.assign({}, values);
      // console.log(medicoToRegister);
      this.clinicaService.registerMedico(medicoToRegister, this.authService.decodedToken.nameid).subscribe(() => {
        this.alert.success('Médico cadastrado com sucesso!');
      }, error => {
        this.alert.error(error);
      }, () => {
        // this.medicos = [...this.medicos, medicoToRegister];
        this.medicos.push(medicoToRegister);
        this.dataSource = new MatTableDataSource(this.medicos);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });
  }

}
