import { Component, OnInit, ViewChild } from '@angular/core';
import { Laboratorio } from '../_models/laboratorio';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSort, MatDialogConfig, MatDialog } from '@angular/material';
import { AlertService } from '../_services/alert.service';
import { AddLaboratorioDialogComponent } from '../dialogs/add-laboratorio-dialog/add-laboratorio-dialog.component';
import { AuthService } from '../_services/auth.service';
import { ClinicaService } from '../_services/clinica.service';

@Component({
  selector: 'app-laboratorios',
  templateUrl: './laboratorios.component.html',
  styleUrls: ['./laboratorios.component.scss']
})
export class LaboratoriosComponent implements OnInit {
  laboratorios: Laboratorio[];
  displayedColumns: string[] = ['nome', 'telefone', 'bairro', 'cidade', 'estado'];
  dataSource: MatTableDataSource<Laboratorio>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  private addLaboratorioConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: true
  };

  constructor(private route: ActivatedRoute, private dialog: MatDialog, private alert: AlertService,
    private clinicaService: ClinicaService, private authService: AuthService) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.laboratorios = data['laboratorios'];
    });
    this.dataSource = new MatTableDataSource(this.laboratorios);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  adicionarLaboratorio() {
    const addLaboratorioConfig = this.addLaboratorioConfig;
    const dialogRef = this.dialog.open(AddLaboratorioDialogComponent, addLaboratorioConfig);
    dialogRef.afterClosed().subscribe(values => {
      if (values === undefined) {
        this.alert.message('Cadastro cancelado');
        return;
      }
      const laboratorioToRegister: Laboratorio = Object.assign({}, values);
      // console.log(medicoToRegister);
      this.clinicaService.registerLaboratorio(laboratorioToRegister, this.authService.decodedToken.nameid).subscribe(() => {
        this.alert.success('Laboratório cadastrado com sucesso!');
      }, error => {
        this.alert.error(error);
      }, () => {
        this.laboratorios.push(laboratorioToRegister);
        this.dataSource = new MatTableDataSource(this.laboratorios);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });
  }

}
