export const menus = [
    {
        'name': 'Dashboard',
        'icon': 'dashboard',
        'link': 'dashboard'
    },
    {
        'name': 'Médicos',
        'icon': 'local_hospital',
        'link': 'medicos'
    },
    {
        'name': 'Pacientes',
        'icon': 'account_box',
        'link': 'pacientes'
    },
    {
        'name': 'Clínicas',
        'icon': 'domain',
        'link': 'clinicas'
    },
    {
        'name': 'Laboratórios',
        'icon': 'local_pharmacy',
        'link': 'laboratorios'
    },
    {
        'name': 'Enviar Laudo',
        'icon': 'cloud_upload',
        'link': 'uploadPdf'
    },
    {
        'name': 'Admin',
        'icon': 'security',
        'link': 'admin'
    }
];
