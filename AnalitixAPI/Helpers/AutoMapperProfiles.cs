using AnalitixAPI.Dtos;
using AnalitixAPI.Models;
using AutoMapper;

namespace AnalitixAPI.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForListDto>();
            CreateMap<Medico, MedicosForDetailedDto>();
            CreateMap<Amostra, AmostrasForDetaildDto>();
            CreateMap<UserForUpdateDto, User>();
            CreateMap<UserForRegisterDto, User>();
            CreateMap<ClinicaForRegisterDto, Clinica>();
            CreateMap<MedicoForRegisterDto, Medico>();
            CreateMap<PacienteForRegisterDto, Paciente>();
            CreateMap<ClinicaForDashboardDto, Clinica>();
            CreateMap<PacienteForClinicaDashboardDto, Paciente>();
            CreateMap<MedicoForClinicaDashboardDto, Medico>();
            CreateMap<LaboratorioForRegisterDto, Laboratorio>();
            CreateMap<MotoboyForRegisterDto, Motoboy>();
        }
    }
}