using System;
using Microsoft.AspNetCore.Http;

namespace AnalitixAPI.Helpers
{
    public static class Extensions
    {
        public static void AddApplicationError(this HttpResponse response, string message)
        {
            response.Headers.Add("Application-Error", message);
            response.Headers.Add("Access-Control-Expose-Headers", "Application-Error");
            response.Headers.Add("Access-Control-Allow-Origin", "*");
        }

        public static int CalcularIdade(this DateTime theDateTime)
        {
            var idade = DateTime.Today.Year - theDateTime.Year;
            if (theDateTime.AddYears(idade) > DateTime.Today) {
                idade--;
            }

            return idade;
        }
    }
}