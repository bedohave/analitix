namespace AnalitixAPI.Models
{
    public class ClinicaMotoboy
    {
        public int ClinicaId { get; set; }
        public Clinica Clinica { get; set; }
        public int MotoboyId { get; set; }
        public Motoboy Motoboy { get; set; }
    }
}