using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnalitixAPI.Models
{
    public class Medico
    {
        [ForeignKey(nameof(User))]
        public int MedicoId { get; set; }
        public User User { get; set; }

        public string Nome { get; set; }
        public string Registro { get; set; }
        public string Especialidade { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public ICollection<Amostra> Amostras { get; set; }

        public ICollection<MedicoPaciente> MedicoPacientes { get; set; }
        public ICollection<ClinicaMedico> ClinicaMedicos { get; set; }
    }
}