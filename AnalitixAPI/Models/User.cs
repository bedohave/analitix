using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace AnalitixAPI.Models
{
    public class User : IdentityUser<int>
    {
        public string KnownAs { get; set; }
        public DateTime Created { get; set; }

        public ICollection<UserRole> UserRoles { get; set; }
    }
}