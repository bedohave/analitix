namespace AnalitixAPI.Models
{
    public class ClinicaLaboratorio
    {
        public int ClinicaId { get; set; }
        public Clinica Clinica { get; set; }
        public int LaboratorioId { get; set; }
        public Laboratorio Laboratorio { get; set; }
    }
}