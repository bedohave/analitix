using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnalitixAPI.Models
{
    public class Paciente
    {
        [ForeignKey(nameof(User))]
        public int PacienteId { get; set; }
        public User User { get; set; }
        
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Sexo { get; set; }
        public DateTime Nascimento { get; set; }
        
        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }

        public string Celular { get; set; }
        
        public ICollection<Amostra> Amostras { get; set; }

        public ICollection<ClinicaPaciente> ClinicaPacientes { get; set; }
        public ICollection<MedicoPaciente> MedicoPacientes { get; set; }
    }
}