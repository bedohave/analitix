using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnalitixAPI.Models
{
    public class Motoboy
    {
        [ForeignKey(nameof(User))]
        public int MotoboyId { get; set; }
        public User User { get; set; }

        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Celular { get; set; }

        public ICollection<ClinicaMotoboy> ClinicaMotoboys { get; set; }
    }
}