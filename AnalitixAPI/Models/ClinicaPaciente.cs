namespace AnalitixAPI.Models
{
    public class ClinicaPaciente
    {
        public int ClinicaId { get; set; }
        public Clinica Clinica { get; set; }
        public int PacienteId { get; set; }
        public Paciente Paciente { get; set; }
    }
}