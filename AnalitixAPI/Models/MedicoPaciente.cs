namespace AnalitixAPI.Models
{
    public class MedicoPaciente
    {
        public int MedicoId { get; set; }
        public Medico Medico { get; set; }
        public int PacienteId { get; set; }
        public Paciente Paciente { get; set; }
    }
}