namespace AnalitixAPI.Models
{
    public class ClinicaMedico
    {
        public int ClinicaId { get; set; }
        public Clinica Clinica { get; set; }
        public int MedicoId { get; set; }
        public Medico Medico { get; set; }
    }
}