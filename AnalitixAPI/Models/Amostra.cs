using System;
using System.ComponentModel.DataAnnotations;

namespace AnalitixAPI.Models
{
    public class Amostra
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string Codigo { get; set; }
        public DateTime DataColeta { get; set; }
        
        public Paciente Paciente { get; set; }
        public int PacienteId { get; set; }
        public Medico Medico { get; set; }
        public int MedicoId { get; set; }
    }
}