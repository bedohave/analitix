using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnalitixAPI.Models
{
    public class Laboratorio
    {
        [ForeignKey(nameof(User))]
        public int LaboratorioId { get; set; }
        public User User { get; set; }

        public string Nome { get; set; }
        public string Telefone { get; set; }

        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }

        public ICollection<ClinicaLaboratorio> ClinicaLaboratorios { get; set; }
    }
}