using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnalitixAPI.Models
{
    public class Clinica
    {
        [ForeignKey(nameof(User))]
        public int ClinicaId { get; set; }
        public User User { get; set; }
        
        public string Nome { get; set; }
        public string RazaoSocial { get; set; }
        public string Cpf { get; set; }
        public string Cnpj { get; set; }

        public int Banco { get; set; }
        public int Agencia { get; set; }
        public int Conta { get; set; }

        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }

        public ICollection<ClinicaLaboratorio> ClinicaLaboratorios { get; set; }
        public ICollection<ClinicaMedico> ClinicaMedicos { get; set; }
        public ICollection<ClinicaMotoboy> ClinicaMotoboys { get; set; }
        public ICollection<ClinicaPaciente> ClinicaPacientes { get; set; }
    }
}