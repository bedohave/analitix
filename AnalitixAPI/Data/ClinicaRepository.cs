using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnalitixAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace AnalitixAPI.Data
{
    public class ClinicaRepository : IClinicaRepository
    {
        private readonly DataContext _context;
        public ClinicaRepository(DataContext context)
        {
            _context = context;

        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }
        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        // USUÁRIOS
        public async Task<User> GetUser(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            return user;
        }
        public async Task<IEnumerable<User>> GetUsers()
        {
            var users = await _context.Users.ToListAsync();
            return users;
        }

        // CLINICAS
        public async Task<Clinica> GetClinica(int id)
        {
            var clinica = await _context.Clinicas.FirstOrDefaultAsync(c => c.ClinicaId == id);
            return clinica;
        }
        public async Task<IEnumerable<Clinica>> GetClinicas()
        {
            var clinicas = await _context.Clinicas.ToListAsync();
            return clinicas;
        }
        public async Task<IEnumerable<Medico>> GetMedicosFromClinica(int id)
        {
            var medicosFromClinica = await _context.Medicos.Where(m => m.MedicoId == id).ToListAsync();
            return medicosFromClinica;
        }
        public async Task<IEnumerable<Paciente>> GetPacientesFromClinica(int id)
        {
            var pacientesFromClinica = await _context.Pacientes.Where(p => p.PacienteId == id).ToListAsync();
            return pacientesFromClinica;
        }

        // MEDICOS
        public async Task<Medico> GetMedico(int id)
        {
            var medico = await _context.Medicos.FirstOrDefaultAsync(m => m.MedicoId == id);
            return medico;
        }
        public async Task<IEnumerable<Medico>> GetMedicos()
        {
            var medicos = await _context.Medicos.ToListAsync();
            return medicos;
        }

        // PACIENTES
        public async Task<Paciente> GetPaciente(int id)
        {
            var paciente = await _context.Pacientes.FirstOrDefaultAsync(p => p.PacienteId == id);
            return paciente;
        }
        public async Task<IEnumerable<Paciente>> GetPacientes()
        {
            var pacientes = await _context.Pacientes.ToListAsync();
            return pacientes;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}