using AnalitixAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AnalitixAPI.Data
{
    public class DataContext : IdentityDbContext<User, Role, int, IdentityUserClaim<int>,
        UserRole, IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        public DataContext(DbContextOptions<DataContext> options) : base (options) {}

        public DbSet<Laboratorio> Laboratorios { get; set; }
        public DbSet<Medico> Medicos { get; set; }
        public DbSet<Motoboy> Motoboys { get; set; }
        public DbSet<Paciente> Pacientes { get; set; }
        public DbSet<Clinica> Clinicas { get; set; }
        public DbSet<Amostra> Amostras { get; set; }
        public DbSet<ClinicaLaboratorio> ClinicaLaboratorios { get; set; }
        public DbSet<ClinicaMedico> ClinicaMedicos { get; set; }
        public DbSet<ClinicaMotoboy> ClinicaMotoboys { get; set; }
        public DbSet<ClinicaPaciente> ClinicaPacientes { get; set; }
        public DbSet<MedicoPaciente> MedicoPacientes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<UserRole>(userRole => {
                userRole.HasKey(ur => new {ur.UserId, ur.RoleId});

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            builder.Entity<ClinicaLaboratorio>().ToTable("ClinicaLaboratorio");
            builder.Entity<ClinicaLaboratorio>(clinicaLaboratorio => {
                clinicaLaboratorio.HasKey(cl => new {cl.ClinicaId, cl.LaboratorioId});

                clinicaLaboratorio.HasOne(cl => cl.Laboratorio)
                    .WithMany(c => c.ClinicaLaboratorios)
                    .HasForeignKey(cl => cl.LaboratorioId)
                    .IsRequired();

                clinicaLaboratorio.HasOne(cl => cl.Clinica)
                    .WithMany(c => c.ClinicaLaboratorios)
                    .HasForeignKey(cl => cl.ClinicaId)
                    .IsRequired();
            });

            builder.Entity<ClinicaMedico>().ToTable("ClinicaMedico");
            builder.Entity<ClinicaMedico>(clinicaMedico => {
                clinicaMedico.HasKey(cm => new {cm.ClinicaId, cm.MedicoId});

                clinicaMedico.HasOne(cm => cm.Medico)
                    .WithMany(m => m.ClinicaMedicos)
                    .HasForeignKey(cm => cm.MedicoId)
                    .IsRequired();

                clinicaMedico.HasOne(cm => cm.Clinica)
                    .WithMany(m => m.ClinicaMedicos)
                    .HasForeignKey(cm => cm.ClinicaId)
                    .IsRequired();
            });

            builder.Entity<ClinicaMotoboy>().ToTable("ClinicaMotoboy");
            builder.Entity<ClinicaMotoboy>(clinicaMotoboy => {
                clinicaMotoboy.HasKey(cm => new {cm.ClinicaId, cm.MotoboyId});

                clinicaMotoboy.HasOne(cm => cm.Motoboy)
                    .WithMany(m => m.ClinicaMotoboys)
                    .HasForeignKey(cm => cm.MotoboyId)
                    .IsRequired();

                clinicaMotoboy.HasOne(cm => cm.Clinica)
                    .WithMany(m => m.ClinicaMotoboys)
                    .HasForeignKey(cm => cm.ClinicaId)
                    .IsRequired();
            });

            builder.Entity<ClinicaPaciente>().ToTable("ClinicaPaciente");
            builder.Entity<ClinicaPaciente>(clinicaPaciente => {
                clinicaPaciente.HasKey(cp => new {cp.ClinicaId, cp.PacienteId});

                clinicaPaciente.HasOne(cp => cp.Paciente)
                    .WithMany(p => p.ClinicaPacientes)
                    .HasForeignKey(cp => cp.PacienteId)
                    .IsRequired();

                clinicaPaciente.HasOne(cp => cp.Clinica)
                    .WithMany(p => p.ClinicaPacientes)
                    .HasForeignKey(cp => cp.ClinicaId)
                    .IsRequired();
            });

            builder.Entity<MedicoPaciente>().ToTable("MedicoPaciente");
            builder.Entity<MedicoPaciente>(medicoPaciente => {
                medicoPaciente.HasKey(mp => new {mp.MedicoId, mp.PacienteId});

                medicoPaciente.HasOne(mp => mp.Paciente)
                    .WithMany(m => m.MedicoPacientes)
                    .HasForeignKey(mp => mp.PacienteId)
                    .IsRequired();

                medicoPaciente.HasOne(mp => mp.Medico)
                    .WithMany(m => m.MedicoPacientes)
                    .HasForeignKey(mp => mp.MedicoId)
                    .IsRequired();
            });
        }

    }
}