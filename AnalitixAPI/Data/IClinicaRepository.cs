using System.Collections.Generic;
using System.Threading.Tasks;
using AnalitixAPI.Models;

namespace AnalitixAPI.Data
{
    public interface IClinicaRepository
    {
        void Add<T>(T entity) where T: class;
        void Delete<T>(T entity) where T: class;

        // USUÁRIOS
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetUser(int id);

        // CLINICAS
        Task<IEnumerable<Clinica>> GetClinicas();
        Task<Clinica> GetClinica(int id);
        Task<IEnumerable<Medico>> GetMedicosFromClinica(int id);
        Task<IEnumerable<Paciente>> GetPacientesFromClinica(int id);
        
        // MEDICOS
        Task<IEnumerable<Medico>> GetMedicos();
        Task<Medico> GetMedico(int id);

        // PACIENTES
        Task<IEnumerable<Paciente>> GetPacientes();
        Task<Paciente> GetPaciente(int id);

        Task<bool> SaveAll();
    }
}