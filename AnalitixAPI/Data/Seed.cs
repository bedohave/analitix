using System.Collections.Generic;
using System.Linq;
using AnalitixAPI.Dtos;
using AnalitixAPI.Models;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace AnalitixAPI.Data
{
    public class Seed
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public Seed(UserManager<User> userManager, RoleManager<Role> roleManager, DataContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public void SeedUsers()
        {
            if (!_userManager.Users.Any())
            {
                var userData = System.IO.File.ReadAllText("Data/UserSeedData.json");
                var users = JsonConvert.DeserializeObject<List<User>>(userData);

                var roles = new List<Role>
                {
                    new Role{Name = "Member"},
                    new Role{Name = "Admin"},
                    new Role{Name = "Moderator"},
                    new Role{Name = "VIP"},
                };

                foreach (var role in roles)
                {
                    _roleManager.CreateAsync(role).Wait();
                }

                // Cria o Admin
                var adminUser = new User
                {
                    UserName = "Admin"
                };

                IdentityResult result = _userManager.CreateAsync(adminUser, "password").Result;

                if (result.Succeeded)
                {
                    var admin = _userManager.FindByNameAsync("Admin").Result;
                    _userManager.AddToRolesAsync(admin, new[] { "Admin", "Moderator" }).Wait();
                }

                // Insere usuários iniciais
                foreach (var user in users)
                {
                    _userManager.CreateAsync(user, "asdd").Wait();
                    _userManager.AddToRoleAsync(user, "Member").Wait();
                }
            }
        }

        public void SeedTPT()
        {
            if (!_userManager.Users.Any())
            {
                var roles = new List<Role>
                {
                    new Role{Name = "Admin"},
                    new Role{Name = "Clinica"},
                    new Role{Name = "Laboratorio"},
                    new Role{Name = "Medico"},
                    new Role{Name = "Motoboy"},
                    new Role{Name = "Paciente"}
                };

                foreach (var role in roles)
                {
                    _roleManager.CreateAsync(role).Wait();
                }

                // Cria o Admin
                var adminUser = new User
                {
                    UserName = "Admin"
                };

                IdentityResult result = _userManager.CreateAsync(adminUser, "asdd").Result;

                if (result.Succeeded)
                {
                    var admin = _userManager.FindByNameAsync("Admin").Result;
                    _userManager.AddToRoleAsync(admin, "Admin").Wait();
                }

                var clinicaData = System.IO.File.ReadAllText("Data/ClinicaSeedData.json");
                var clinicas = JsonConvert.DeserializeObject<List<Clinica>>(clinicaData);

                foreach (Clinica clinica in clinicas)
                {
                    _context.Clinicas.Add(clinica);
                    IdentityResult clinicaResult = _userManager.CreateAsync(clinica.User, "asdd").Result;
                    if (clinicaResult.Succeeded)
                    {
                        _userManager.AddToRoleAsync(clinica.User, "Clinica").Wait();
                    }
                }

                var laboratorioData = System.IO.File.ReadAllText("Data/LaboratorioSeedData.json");
                var laboratorios = JsonConvert.DeserializeObject<List<Laboratorio>>(laboratorioData);

                foreach (Laboratorio laboratorio in laboratorios)
                {
                    _context.Laboratorios.Add(laboratorio);
                    IdentityResult laboratorioResult = _userManager.CreateAsync(laboratorio.User, "asdd").Result;
                    if (laboratorioResult.Succeeded)
                    {
                        _userManager.AddToRoleAsync(laboratorio.User, "Laboratorio").Wait();
                    }
                }

                var medicoData = System.IO.File.ReadAllText("Data/MedicoSeedData.json");
                var medicos = JsonConvert.DeserializeObject<List<Medico>>(medicoData);

                foreach (Medico medico in medicos)
                {
                    _context.Medicos.Add(medico);
                    IdentityResult medicoResult = _userManager.CreateAsync(medico.User, "asdd").Result;
                    if (medicoResult.Succeeded)
                    {
                        _userManager.AddToRoleAsync(medico.User, "Medico").Wait();
                    }
                }

                var motoboyData = System.IO.File.ReadAllText("Data/MotoboySeedData.json");
                var motoboys = JsonConvert.DeserializeObject<List<Motoboy>>(motoboyData);

                foreach (Motoboy motoboy in motoboys)
                {
                    _context.Motoboys.Add(motoboy);
                    IdentityResult motoboyResult = _userManager.CreateAsync(motoboy.User, "asdd").Result;
                    if (motoboyResult.Succeeded)
                    {
                        _userManager.AddToRoleAsync(motoboy.User, "Motoboy").Wait();
                    }
                }

                var pacienteData = System.IO.File.ReadAllText("Data/PacienteSeedData.json");
                var pacientes = JsonConvert.DeserializeObject<List<Paciente>>(pacienteData);

                foreach (Paciente paciente in pacientes)
                {
                    _context.Pacientes.Add(paciente);
                    IdentityResult pacienteResult = _userManager.CreateAsync(paciente.User, "asdd").Result;
                    if (pacienteResult.Succeeded)
                    {
                        _userManager.AddToRoleAsync(paciente.User, "Paciente").Wait();
                    }
                }

                var clinicaLaboratorios = new ClinicaLaboratorio[]
                {
                    new ClinicaLaboratorio {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica1").ClinicaId,
                        LaboratorioId = laboratorios.Single(l => l.User.UserName == "laboratorio1").LaboratorioId
                    },
                    new ClinicaLaboratorio {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica1").ClinicaId,
                        LaboratorioId = laboratorios.Single(l => l.User.UserName == "laboratorio2").LaboratorioId
                    },
                    new ClinicaLaboratorio {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica2").ClinicaId,
                        LaboratorioId = laboratorios.Single(l => l.User.UserName == "laboratorio3").LaboratorioId
                    }
                };
                foreach (ClinicaLaboratorio cl in clinicaLaboratorios)
                {
                    _context.ClinicaLaboratorios.Add(cl);
                }
                _context.SaveChanges();

                var clinicaMedicos = new ClinicaMedico[]
                {
                    new ClinicaMedico {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica1").ClinicaId,
                        MedicoId = medicos.Single(m => m.User.UserName == "drarmando").MedicoId
                    },
                    new ClinicaMedico {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica1").ClinicaId,
                        MedicoId = medicos.Single(m => m.User.UserName == "drrodrigo").MedicoId
                    },
                    new ClinicaMedico {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica2").ClinicaId,
                        MedicoId = medicos.Single(m => m.User.UserName == "drdavid").MedicoId
                    },
                    new ClinicaMedico {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica2").ClinicaId,
                        MedicoId = medicos.Single(m => m.User.UserName == "drdiego").MedicoId
                    }
                };
                foreach (ClinicaMedico cm in clinicaMedicos)
                {
                    _context.ClinicaMedicos.Add(cm);
                }
                _context.SaveChanges();

                var clinicaMotoboys = new ClinicaMotoboy[]
                {
                    new ClinicaMotoboy {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica1").ClinicaId,
                        MotoboyId = motoboys.Single(m => m.User.UserName == "motoboy1").MotoboyId
                    },
                    new ClinicaMotoboy {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica1").ClinicaId,
                        MotoboyId = motoboys.Single(m => m.User.UserName == "motoboy4").MotoboyId
                    },
                    new ClinicaMotoboy {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica2").ClinicaId,
                        MotoboyId = motoboys.Single(m => m.User.UserName == "motoboy2").MotoboyId
                    },
                    new ClinicaMotoboy {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica2").ClinicaId,
                        MotoboyId = motoboys.Single(m => m.User.UserName == "motoboy3").MotoboyId
                    }
                };
                foreach (ClinicaMotoboy cm in clinicaMotoboys)
                {
                    _context.ClinicaMotoboys.Add(cm);
                }
                _context.SaveChanges();

                var clinicaPacientes = new ClinicaPaciente[]
                {
                    new ClinicaPaciente {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica1").ClinicaId,
                        PacienteId = pacientes.Single(p => p.User.UserName == "lola").PacienteId
                    },
                    new ClinicaPaciente {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica1").ClinicaId,
                        PacienteId = pacientes.Single(p => p.User.UserName == "bob").PacienteId
                    },
                    new ClinicaPaciente {
                        ClinicaId = clinicas.Single(c => c.User.UserName == "clinica2").ClinicaId,
                        PacienteId = pacientes.Single(p => p.User.UserName == "luke").PacienteId
                    }
                };
                foreach (ClinicaPaciente cp in clinicaPacientes)
                {
                    _context.ClinicaPacientes.Add(cp);
                }
                _context.SaveChanges();

                var medicoPacientes = new MedicoPaciente[]
                {
                    new MedicoPaciente {
                        MedicoId = medicos.Single(m => m.User.UserName == "drarmando").MedicoId,
                        PacienteId = pacientes.Single(p => p.User.UserName == "lola").PacienteId
                    },
                    new MedicoPaciente {
                        MedicoId = medicos.Single(m => m.User.UserName == "drrodrigo").MedicoId,
                        PacienteId = pacientes.Single(p => p.User.UserName == "bob").PacienteId
                    },
                    new MedicoPaciente {
                        MedicoId = medicos.Single(m => m.User.UserName == "drdavid").MedicoId,
                        PacienteId = pacientes.Single(p => p.User.UserName == "bob").PacienteId
                    },
                    new MedicoPaciente {
                        MedicoId = medicos.Single(m => m.User.UserName == "drdiego").MedicoId,
                        PacienteId = pacientes.Single(p => p.User.UserName == "bob").PacienteId
                    }
                };
                foreach (MedicoPaciente mp in medicoPacientes)
                {
                    _context.MedicoPacientes.Add(mp);
                }
                _context.SaveChanges();
            }
        }
    }
}