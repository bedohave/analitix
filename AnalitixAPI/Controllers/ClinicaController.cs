using System.Threading.Tasks;
using AnalitixAPI.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AnalitixAPI.Dtos;
using AnalitixAPI.Models;
using Microsoft.AspNetCore.Identity;

namespace AnalitixAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ClinicaController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IClinicaRepository _repo;
        private readonly UserManager<User> _userManager;
        public ClinicaController(DataContext context, IClinicaRepository repo, IMapper mapper, UserManager<User> userManager)
        {
            _userManager = userManager;
            _context = context;
            _mapper = mapper;
            _repo = repo;
        }

        [Authorize(Policy = "RequireClinicaRole")]
        [HttpGet("laboratorios/{id}")]
        public async Task<IActionResult> GetLaboratorios(int id)
        {
            var laboratoriosToReturn = await (from clinicaLaboratorio in _context.ClinicaLaboratorios
                                              where clinicaLaboratorio.ClinicaId.Equals(id)
                                              join laboratorio in _context.Laboratorios
                                              on clinicaLaboratorio.LaboratorioId
                                              equals laboratorio.LaboratorioId
                                              select new
                                              {
                                                  Nome = laboratorio.Nome,
                                                  Telefone = laboratorio.Telefone,
                                                  Cep = laboratorio.Cep,
                                                  Endereco = laboratorio.Endereco,
                                                  Bairro = laboratorio.Bairro,
                                                  Cidade = laboratorio.Cidade,
                                                  Estado = laboratorio.Estado

                                              }).ToListAsync();
            return Ok(laboratoriosToReturn);
        }

        [Authorize(Policy = "RequireClinicaRole")]
        [HttpPost("laboratorio/{id}")]
        public async Task<IActionResult> RegisterMedico(int id, LaboratorioForRegisterDto laboratorio)
        {
            var laboratorioToCreate = _mapper.Map<Laboratorio>(laboratorio);
            var result = await _userManager.CreateAsync(laboratorioToCreate.User, laboratorio.User.Password);

            if (result.Succeeded)
            {
                _userManager.AddToRoleAsync(laboratorioToCreate.User, "Medico").Wait();
                _context.Laboratorios.Update(laboratorioToCreate);
                _context.SaveChanges();
            }

            var userToReturn = _mapper.Map<UserForDetailedDto>(laboratorioToCreate.User);

            if (result.Succeeded)
            {
                var clinicaLaboratorio = new ClinicaLaboratorio
                {
                    ClinicaId = id,
                    LaboratorioId = laboratorioToCreate.User.Id
                };
                _context.ClinicaLaboratorios.Add(clinicaLaboratorio);
                _context.SaveChanges();

                return Ok();
            }

            return BadRequest(result.Errors);
        }

        [Authorize(Policy = "RequireClinicaRole")]
        [HttpGet("medicos/{id}")]
        public async Task<IActionResult> GetMedicos(int id)
        {
            var medicosToReturn = await (from clinicaMedico in _context.ClinicaMedicos
                                         where clinicaMedico.ClinicaId.Equals(id)
                                         join medico in _context.Medicos
                                         on clinicaMedico.MedicoId
                                         equals medico.MedicoId
                                         select new
                                         {
                                             Nome = medico.Nome,
                                             Registro = medico.Registro,
                                             Especialidade = medico.Especialidade,
                                             Cidade = medico.Cidade,
                                             Estado = medico.Estado
                                         }).ToListAsync();
            return Ok(medicosToReturn);
        }

        [Authorize(Policy = "RequireClinicaRole")]
        [HttpPost("medico/{id}")]
        public async Task<IActionResult> RegisterMedico(int id, MedicoForRegisterDto medico)
        {
            var medicoToCreate = _mapper.Map<Medico>(medico);
            var result = await _userManager.CreateAsync(medicoToCreate.User, medico.User.Password);

            if (result.Succeeded)
            {
                _userManager.AddToRoleAsync(medicoToCreate.User, "Medico").Wait();
                _context.Medicos.Update(medicoToCreate);
                _context.SaveChanges();
            }

            var userToReturn = _mapper.Map<UserForDetailedDto>(medicoToCreate.User);

            if (result.Succeeded)
            {
                var clinicaMedico = new ClinicaMedico
                {
                    ClinicaId = id,
                    MedicoId = medicoToCreate.User.Id
                };
                _context.ClinicaMedicos.Add(clinicaMedico);
                _context.SaveChanges();

                return Ok();
            }

            return BadRequest(result.Errors);
        }

        [Authorize(Policy = "RequireClinicaRole")]
        [HttpGet("motoboys/{id}")]
        public async Task<IActionResult> GetMotoboys(int id)
        {
            var motoboysToReturn = await (from clinicaMotoboy in _context.ClinicaMotoboys
                                          where clinicaMotoboy.ClinicaId.Equals(id)
                                          join motoboy in _context.Motoboys
                                          on clinicaMotoboy.MotoboyId
                                          equals motoboy.MotoboyId
                                          select new
                                          {
                                              Nome = motoboy.Nome,
                                              Cpf = motoboy.Cpf,
                                              Celular = motoboy.Celular

                                          }).ToListAsync();
            return Ok(motoboysToReturn);
        }

        [Authorize(Policy = "RequireClinicaRole")]
        [HttpGet("pacientes/{id}")]
        public async Task<IActionResult> GetPacientes(int id)
        {
            var pacientesToReturn = await (from clinicaPaciente in _context.ClinicaPacientes
                                           where clinicaPaciente.ClinicaId.Equals(id)
                                           join paciente in _context.Pacientes
                                           on clinicaPaciente.PacienteId
                                           equals paciente.PacienteId
                                           select new
                                           {
                                               Nome = paciente.Nome,
                                               Cpf = paciente.Cpf,
                                               Sexo = paciente.Sexo,
                                               Nascimento = paciente.Nascimento,
                                               Cep = paciente.Cep,
                                               Endereco = paciente.Endereco,
                                               Bairro = paciente.Bairro,
                                               Cidade = paciente.Cidade,
                                               Estado = paciente.Estado,
                                               Celular = paciente.Celular
                                           }).ToListAsync();
            return Ok(pacientesToReturn);
        }

        [Authorize(Policy = "RequireClinicaRole")]
        [HttpGet("{id}", Name = "GetClinica")]
        public async Task<IActionResult> GetClinica(int id)
        {
            var clinicaToReturn = await (from clinica in _context.Clinicas
                                         where clinica.ClinicaId.Equals(id)
                                         orderby clinica.User.KnownAs
                                         select new
                                         {
                                             Id = clinica.ClinicaId,
                                             Nome = clinica.Nome,
                                             RazaoSocial = clinica.RazaoSocial,
                                             Cpf = clinica.Cpf,
                                             Cnpj = clinica.Cnpj,
                                             Banco = clinica.Banco,
                                             Agencia = clinica.Agencia,
                                             Conta = clinica.Conta,
                                             Cep = clinica.Cep,
                                             Endereco = clinica.Endereco,
                                             Bairro = clinica.Bairro,
                                             Cidade = clinica.Cidade,
                                             Estado = clinica.Estado,
                                             Laboratorios = (from clinicaLaboratorio in clinica.ClinicaLaboratorios
                                                             join laboratorio in _context.Laboratorios
                                                             on clinicaLaboratorio.LaboratorioId
                                                             equals laboratorio.LaboratorioId
                                                             select new
                                                             {
                                                                 Nome = laboratorio.Nome,
                                                                 Telefone = laboratorio.Telefone,
                                                                 Cep = laboratorio.Cep,
                                                                 Endereco = laboratorio.Endereco,
                                                                 Bairro = laboratorio.Bairro,
                                                                 Cidade = laboratorio.Cidade,
                                                                 Estado = laboratorio.Estado

                                                             }).ToList(),
                                             Medicos = (from clinicaMedico in clinica.ClinicaMedicos
                                                        join medico in _context.Medicos
                                                        on clinicaMedico.MedicoId
                                                        equals medico.MedicoId
                                                        select new
                                                        {
                                                            Nome = medico.Nome,
                                                            Registro = medico.Registro,
                                                            Especialidade = medico.Especialidade,
                                                            Cidade = medico.Cidade,
                                                            Estado = medico.Estado
                                                        }).ToList(),
                                             Motoboys = (from clinicaMotoboy in clinica.ClinicaMotoboys
                                                         join motoboy in _context.Motoboys
                                                         on clinicaMotoboy.MotoboyId
                                                         equals motoboy.MotoboyId
                                                         select new
                                                         {
                                                             Nome = motoboy.Nome,
                                                             Cpf = motoboy.Cpf,
                                                             Celular = motoboy.Celular

                                                         }).ToList(),
                                             Pacientes = (from clinicaPaciente in clinica.ClinicaPacientes
                                                          join paciente in _context.Pacientes
                                                          on clinicaPaciente.PacienteId
                                                          equals paciente.PacienteId
                                                          select new
                                                          {
                                                              Nome = paciente.Nome,
                                                              Cpf = paciente.Cpf,
                                                              Sexo = paciente.Sexo,
                                                              Cep = paciente.Cep,
                                                              Endereco = paciente.Endereco,
                                                              Bairro = paciente.Bairro,
                                                              Cidade = paciente.Cidade,
                                                              Estado = paciente.Estado,
                                                              Celular = paciente.Celular
                                                          }).ToList()
                                         }).FirstOrDefaultAsync();
            return Ok(clinicaToReturn);
        }
    }
}