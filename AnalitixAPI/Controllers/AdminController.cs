using System.Threading.Tasks;
using AnalitixAPI.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AnalitixAPI.Dtos;
using Microsoft.AspNetCore.Identity;
using AnalitixAPI.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AnalitixAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AdminController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;

        public AdminController(DataContext context, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            _roleManager = roleManager;
            _context = context;
            _userManager = userManager;
        }

        [Authorize(Policy = "RequireAdminRole")]
        [HttpGet("usersWithRoles")]
        public async Task<IActionResult> GetUsersWithRoles()
        {
            var userList = await (from user in _context.Users
                                  orderby user.UserName
                                  select new
                                  {
                                      Id = user.Id,
                                      UserName = user.UserName,
                                      Roles = (from userRole in user.UserRoles
                                               join role in _context.Roles
                                               on userRole.RoleId
                                               equals role.Id
                                               select role.Name).ToList()
                                  }).ToListAsync();
            return Ok(userList);
        }

        [Authorize(Policy = "RequireAdminRole")]
        [HttpPost("editRoles/{userName}")]
        public async Task<IActionResult> EditRoles(string userName, RoleEditDto roleEditDto)
        {
            var user = await _userManager.FindByNameAsync(userName);
            var userRoles = await _userManager.GetRolesAsync(user);

            var selectedRoles = roleEditDto.RoleNames;
            selectedRoles = selectedRoles ?? new string[] { };

            var result = await _userManager.AddToRolesAsync(user, selectedRoles.Except(userRoles));
            if (!result.Succeeded) return BadRequest("Failed to add to roles");

            result = await _userManager.RemoveFromRolesAsync(user, userRoles.Except(selectedRoles));
            if (!result.Succeeded) return BadRequest("Failed to remove the roles");

            return Ok(await _userManager.GetRolesAsync(user));
        }

        [Authorize(Policy = "RequireClinicaRole")]
        [HttpGet("usersForClinica")]
        public IActionResult GetUsersForClinica()
        {
            return Ok("Admins ou Clinicas podem ver isso");
        }

        [Authorize(Policy = "RequireAdminRole")]
        [HttpGet("roles")]
        public async Task<IActionResult> GetRoles()
        {
            var rolesList = await (from role in _context.Roles
                                    orderby role.Name
                                    select new{
                                        name = role.Name,
                                        value = role.Name
                                    }).ToListAsync();
            return Ok(rolesList);
        }

        [Authorize(Policy = "RequireClinicaRole")]
        [HttpGet("clinicas")]
        public async Task<IActionResult> GetClinicas()
        {
            var clinicaList = await (from clinica in _context.Clinicas
                                  orderby clinica.User.KnownAs
                                  select new
                                  {
                                      Id = clinica.ClinicaId,
                                      RazaoSocial = clinica.RazaoSocial,
                                      Nome = clinica.User.KnownAs,
                                      Medicos = (from clinicaMedico in clinica.ClinicaMedicos
                                                join medico in _context.Medicos
                                                on clinicaMedico.MedicoId
                                                equals medico.MedicoId
                                                select new
                                                {
                                                    Nome = medico.User.KnownAs,
                                                    Registro = medico.Registro,
                                                    Especialidade = medico.Especialidade
                                                }).ToList(),
                                      Pacientes = (from clinicaPaciente in clinica.ClinicaPacientes
                                                join paciente in _context.Pacientes
                                                on clinicaPaciente.PacienteId
                                                equals paciente.PacienteId
                                                select new
                                                {
                                                    Nome = paciente.User.KnownAs,
                                                    Sexo = paciente.Sexo
                                                }).ToList()
                                  }).ToListAsync();
            return Ok(clinicaList);
        }

    }
}