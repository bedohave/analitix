using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AnalitixAPI.Data;
using AnalitixAPI.Dtos;
using AnalitixAPI.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace AnalitixAPI.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IClinicaRepository _repo;
        private readonly DataContext _context;
        public AuthController(IConfiguration config, IMapper mapper, UserManager<User> userManager, SignInManager<User> signInManager, IClinicaRepository repo, DataContext context)
        {
            _context = context;
            _repo = repo;
            _signInManager = signInManager;
            _userManager = userManager;
            _mapper = mapper;
            _config = config;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginDto userForLoginDto)
        {
            var user = await _userManager.FindByNameAsync(userForLoginDto.UserName);

            if (user == null) return Unauthorized();

            var result = await _signInManager.CheckPasswordSignInAsync(user, userForLoginDto.Password, false);

            if (result.Succeeded)
            {
                var appUser = await _userManager.Users
                    .FirstOrDefaultAsync(u => u.NormalizedUserName == userForLoginDto.UserName.ToUpper());

                var userToReturn = _mapper.Map<UserForListDto>(appUser);

                return Ok(new
                {
                    token = GenerateJwtToken(appUser).Result,
                    user = userToReturn
                });
            }

            return Unauthorized();
        }

        // [HttpPost("register")]
        // public async Task<IActionResult> Register(UserForRegisterDto userForRegisterDto)
        // {
        //     var userToCreate = _mapper.Map<User>(userForRegisterDto);

        //     var result = await _userManager.CreateAsync(userToCreate, userForRegisterDto.Password);

        //     var userToReturn = _mapper.Map<UserForDetailedDto>(userToCreate);

        //     if (result.Succeeded)
        //     {
        //         return CreatedAtRoute("GetUser", new { controller = "Users", id = userToCreate.Id }, userToReturn);
        //     }

        //     return BadRequest(result.Errors);
        // }

        [HttpPost("register/clinica")]
        public async Task<IActionResult> RegisterClinica(ClinicaForRegisterDto clinica)
        {
            var clinicaToCreate = _mapper.Map<Clinica>(clinica);
            var result = await _userManager.CreateAsync(clinicaToCreate.User, clinica.User.Password);

            if (result.Succeeded)
            {
                _userManager.AddToRoleAsync(clinicaToCreate.User, "Clinica").Wait();
                _context.Clinicas.Update(clinicaToCreate);
                _context.SaveChanges();
            }

            var userToReturn = _mapper.Map<UserForDetailedDto>(clinicaToCreate.User);

            if (result.Succeeded)
            {
                return CreatedAtRoute("GetUser", new { controller = "Users", id = clinicaToCreate.User.Id }, userToReturn);
            }

            return BadRequest(result.Errors);
        }

        [HttpPost("register/laboratorio")]
        public async Task<IActionResult> RegisterLaboratorio(LaboratorioForRegisterDto laboratorio)
        {
            var laboratorioToCreate = _mapper.Map<Laboratorio>(laboratorio);
            var result = await _userManager.CreateAsync(laboratorioToCreate.User, laboratorio.User.Password);

            if (result.Succeeded)
            {
                _userManager.AddToRoleAsync(laboratorioToCreate.User, "Laboratorio").Wait();
                _context.Laboratorios.Update(laboratorioToCreate);
                _context.SaveChanges();
            }

            var userToReturn = _mapper.Map<UserForDetailedDto>(laboratorioToCreate.User);

            if (result.Succeeded)
            {
                return CreatedAtRoute("GetUser", new { controller = "Users", id = laboratorioToCreate.User.Id }, userToReturn);
            }

            return BadRequest(result.Errors);
        }

        [HttpPost("register/medico")]
        public async Task<IActionResult> RegisterMedico(MedicoForRegisterDto medico)
        {
            var medicoToCreate = _mapper.Map<Medico>(medico);
            var result = await _userManager.CreateAsync(medicoToCreate.User, medico.User.Password);

            if (result.Succeeded)
            {
                _userManager.AddToRoleAsync(medicoToCreate.User, "Medico").Wait();
                _context.Medicos.Update(medicoToCreate);
                _context.SaveChanges();
            }

            var userToReturn = _mapper.Map<UserForDetailedDto>(medicoToCreate.User);

            if (result.Succeeded)
            {
                return CreatedAtRoute("GetUser", new { controller = "Users", id = medicoToCreate.User.Id }, userToReturn);
            }

            return BadRequest(result.Errors);
        }

        [HttpPost("register/motoboy")]
        public async Task<IActionResult> RegisterMotoboy(MotoboyForRegisterDto motoboy)
        {
            var motoboyToCreate = _mapper.Map<Motoboy>(motoboy);
            var result = await _userManager.CreateAsync(motoboyToCreate.User, motoboy.User.Password);

            if (result.Succeeded)
            {
                _userManager.AddToRoleAsync(motoboyToCreate.User, "Motoboy").Wait();
                _context.Motoboys.Update(motoboyToCreate);
                _context.SaveChanges();
            }

            var userToReturn = _mapper.Map<UserForDetailedDto>(motoboyToCreate.User);

            if (result.Succeeded)
            {
                return CreatedAtRoute("GetUser", new { controller = "Users", id = motoboyToCreate.User.Id }, userToReturn);
            }

            return BadRequest(result.Errors);
        }

        [HttpPost("register/paciente")]
        public async Task<IActionResult> RegisterPaciente(PacienteForRegisterDto paciente)
        {
            var pacienteToCreate = _mapper.Map<Paciente>(paciente);
            var result = await _userManager.CreateAsync(pacienteToCreate.User, paciente.User.Password);

            if (result.Succeeded)
            {
                _userManager.AddToRoleAsync(pacienteToCreate.User, "Paciente").Wait();
                _context.Pacientes.Update(pacienteToCreate);
                _context.SaveChanges();
            }

            var userToReturn = _mapper.Map<UserForDetailedDto>(pacienteToCreate.User);

            if (result.Succeeded)
            {
                return CreatedAtRoute("GetUser", new { controller = "Users", id = pacienteToCreate.User.Id }, userToReturn);
            }

            return BadRequest(result.Errors);
        }

        private async Task<string> GenerateJwtToken(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName)
            };

            var roles = await _userManager.GetRolesAsync(user);

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8
                .GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.Add(TimeSpan.FromHours(1)),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}