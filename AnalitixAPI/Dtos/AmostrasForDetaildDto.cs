using System;

namespace AnalitixAPI.Dtos
{
    public class AmostrasForDetaildDto
    {
        public int Id { get; set; }
        public string Medico { get; set; }
        public string Tipo { get; set; }
        public string Codigo { get; set; }
        public DateTime DataColeta { get; set; }
    }
}