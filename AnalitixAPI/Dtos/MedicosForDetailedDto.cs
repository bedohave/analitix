namespace AnalitixAPI.Dtos
{
    public class MedicosForDetailedDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Registro { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Especialidade { get; set; }
    }
}