namespace AnalitixAPI.Dtos
{
    public class MotoboyForRegisterDto
    {
        public UserForRegisterDto User { get; set; }

        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Celular { get; set; }
    }
}