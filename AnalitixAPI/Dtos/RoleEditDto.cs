namespace AnalitixAPI.Dtos
{
    public class RoleEditDto
    {
        public string[] RoleNames { get; set; }
    }
}