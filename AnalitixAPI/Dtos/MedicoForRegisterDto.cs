namespace AnalitixAPI.Dtos
{
    public class MedicoForRegisterDto
    {
        public UserForRegisterDto User { get; set; }
        public string Nome { get; set; }
        public string Registro { get; set; }
        public string Especialidade { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
    }
}