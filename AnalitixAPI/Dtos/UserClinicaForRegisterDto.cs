using AnalitixAPI.Models;

namespace AnalitixAPI.Dtos
{
    public class UserClinicaForRegisterDto
    {
        public UserForRegisterDto User { get; set; }
        public ClinicaForRegisterDto Clinica { get; set; }
    }
}