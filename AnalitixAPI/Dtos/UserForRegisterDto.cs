using System;
using System.ComponentModel.DataAnnotations;

namespace AnalitixAPI.Dtos
{
    public class UserForRegisterDto
    {
        // [Required]
        public string UserName { get; set; }

        [Required]
        [StringLength(8, MinimumLength = 4, ErrorMessage = "You must specify a password between 4 and 8 characters!")]
        public string Password { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Email inválido.")]
        public string Email { get; set; }

        public DateTime Created { get; set; }
        public UserForRegisterDto()
        {
            Created = DateTime.Now;
        }
    }
}