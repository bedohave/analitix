namespace AnalitixAPI.Dtos
{
    public class UserForUpdateDto
    {
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Cep { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Endereco { get; set; }
    }
}