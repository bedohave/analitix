using AnalitixAPI.Models;

namespace AnalitixAPI.Dtos
{
    public class LaboratorioForRegisterDto
    {
        public UserForRegisterDto User { get; set; }

        public string Nome { get; set; }
        public string Telefone { get; set; }

        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
    }
}