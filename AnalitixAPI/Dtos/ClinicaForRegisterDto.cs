using AnalitixAPI.Models;

namespace AnalitixAPI.Dtos
{
    public class ClinicaForRegisterDto
    {
        public UserForRegisterDto User { get; set; }
        public string Nome { get; set; }
        public string RazaoSocial { get; set; }
        public string Cpf { get; set; }
        public string Cnpj { get; set; }

        public int Banco { get; set; }
        public int Agencia { get; set; }
        public int Conta { get; set; }

        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
    }
}