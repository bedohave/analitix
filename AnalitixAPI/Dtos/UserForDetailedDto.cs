using System.Collections.Generic;
using AnalitixAPI.Models;

namespace AnalitixAPI.Dtos
{
    public class UserForDetailedDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}